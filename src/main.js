import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import router from './router'
import store from './store'
import {firebase} from './firebase'
/*import './style/custom.scss'*/
import './registerServiceWorker'

Vue.use(VueResource);
Vue.http.options.root = process.env.VUE_APP_FIREBASE_ROOT;

Vue.config.productionTip = false;

const unsubscribe = firebase.auth()
    .onAuthStateChanged((firebaseUser) => {
      new Vue({
        el: '#app',
        router,
        store,
        render: h => h(App),
        created() {
          if (firebaseUser) {
            store.dispatch('autoSignIn', firebaseUser)
          }
        }
      });
      unsubscribe();
    });
