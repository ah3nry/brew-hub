import firebase from 'firebase';
//import 'firebase/firestore';

const settings = {timestampsInSnapshots: true};
//firestore.settings(settings);
//firebase.firestore.setLogLevel('debug');
// Initialize Firebase
firebase.initializeApp({
  apiKey: "AIzaSyAdvzhd389A7PE1-KFaIDcDIL4vM6g2-XI",
  authDomain: "brew-52aad.firebaseapp.com",
  databaseURL: "https://brew-52aad.firebaseio.com",
  projectId: "brew-52aad",
  storageBucket: "brew-52aad.appspot.com",
  messagingSenderId: "534460372432"
});

firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
    .then(function() {
      // Indicates that the state will be persisted even when the browser window
      // is closed or the activity is destroyed in React Native. An explicit
      // sign out is needed to clear that state. Note that Firebase Auth web
      // sessions are single host origin and will be persisted for a single domain only.
      return firebase.auth().signInWithEmailAndPassword(email, password);
    })
    .catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
    });

export {firebase};