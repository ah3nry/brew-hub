import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'

Vue.use(Vuex);
import {firebase} from './firebase';
const database = firebase.firestore();

export default new Vuex.Store({
  state: {
    user: null,
    error: null,
    loading: false,
    recipes: [],
    recipesLoaded: false
  },

  mutations: {
    setUser (state, payload) {
      state.user = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setRecipes (state, recipes) {
      state.recipes = recipes;
    },
    fetchedRecipes (state) {
      state.recipesLoaded = true;
    }
  },

  actions: {
    userSignUp ({commit}, payload) {
      commit('setLoading', true);
      const firebaseUser = firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password);
      firebase.auth().currentUser.updateProfile({
        displayName: payload.username,
      }).then(() => {
          commit('setUser', {username: firebaseUser.user.displayName});
          commit('setLoading', false);
          router.push('Home');
          commit('setError', null);
        })
        .catch(error => {
          commit('setError', error.message);
          commit('setLoading', false);
        })
    },
    userSignIn ({commit}, payload) {
      commit('setLoading', true);
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
          .then(firebaseUser => {
            commit('setUser', {username: firebaseUser.user.displayName});
            commit('setLoading', false);
            commit('setError', null);
            router.push('my-recipes');
          })
          .catch(error => {
            commit('setError', error.message);
            commit('setLoading', false);
          })
    },
    autoSignIn ({commit}, payload) {
      commit('setUser', {username: payload.displayName})
    },
    userSignOut ({commit}) {
      firebase.auth().signOut();
      commit('setUser', null);
      router.push('login');
    },
    loadRecipes: ({commit}) => {
      database.collection('/recipes').get()
          .then(data => {
            if (data) {
              let recipes = [];
              data.forEach((doc) => {
                recipes.push(Object.assign({}, {docId: doc.id}, doc.data()));
              });
              commit('setRecipes', recipes);
              commit('fetchedRecipes');

            }
          })
    }
  },

  getters: {
    isAuthenticated: state => state.user !== null && state.user !== undefined,
    user: state => state.user,
    recipes: state => state.recipes,
    recipesLoaded: state => state.recipesLoaded
  }
})
