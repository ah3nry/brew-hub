import Vue from 'vue'
import Router from 'vue-router';
import NotFound from './views/NotFound';
import firebase from 'firebase';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import(/* webpackChunkName: "home" */ './views/Home.vue')
    },
    {
      path: '/recipe/:docId',
      name: 'Recipe',
      component: () => import(/* webpackChunkName: "recipe" */ './views/Recipe.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import(/* webpackChunkName: "login" */ './views/Login.vue')
    },
    {
      path: '/sign-up',
      name: 'SignUp',
      component: () => import(/* webpackChunkName: "signUp" */ './views/SignUp.vue')
    },
    {
      path: '/my-recipes',
      name: 'MyRecipes',
      component: () => import(/* webpackChunkName: "myRecipes" */ './views/MyRecipes.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '*',
      component: NotFound
    },
    {
      path: '/',
      redirect: '/Home'
    },
  ]
});

router.beforeEach((to, from, next) => {
  let currentUser = firebase.auth().currentUser;
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) next('login');
  //else if (!requiresAuth && currentUser) next('hello');
  else next()
});

export default router;