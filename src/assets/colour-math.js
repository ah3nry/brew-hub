export const colourMath = {
  methods: {
    ebcToSrm(ebc) {
      return ebc * 0.508;
    },
    srmToEbc(srm) {
      return srm * 1.97;
    },
    lovibondToSrm(lovibond) {
      return  (1.3546 * lovibond) - 0.76;
    },
    srmToLovibond(srm) {
      return (srm + 0.76) / 1.3546;
    }
  }
};

